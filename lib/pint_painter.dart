
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class PintPainter extends CustomPainter{

  Offset curOffset;

  Paint _paint;
  PintPainter(Offset offset){
    curOffset = offset;
    _paint =Paint()
      ..color = Colors.red
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;
  }



  @override
  void paint(Canvas canvas,Size size){

    canvas.drawCircle(curOffset, 20, _paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }


}