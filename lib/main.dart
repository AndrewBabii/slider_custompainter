import 'package:flutter/material.dart';
import 'package:slider_custompainter/awesome_painter.dart';
import 'package:slider_custompainter/pint_painter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _currentPosition = 0;

  Offset stOffset;


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: GestureDetector(
          onHorizontalDragUpdate:(details){

            print(details.globalPosition.dx);
            setState(() {
              _currentPosition = details.globalPosition.dx;

              stOffset = Offset(details.globalPosition.dx, MediaQuery.of(context).size.height*0.45);
            });
          },
          onTapDown: (details){

            print(details.globalPosition.dx);
            setState(() {
              _currentPosition = details.globalPosition.dx;

              stOffset = Offset(details.globalPosition.dx, MediaQuery.of(context).size.height*0.45);

            });
          },
            child: Container(
              width: double.infinity,
              height: double.infinity,
              child: CustomPaint(
                willChange: true,
                size:MediaQuery.of(context).size,
                painter: PintPainter(stOffset ?? Offset(MediaQuery.of(context).size.width*0.125,MediaQuery.of(context).size.height*0.45)),
                child: CustomPaint(
                  size: MediaQuery.of(context).size,
                  painter: AwesomePainter(),
                  child: Text(
                    '$_currentPosition',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ),
            ),
          ),
      ),

    );
  }
}
